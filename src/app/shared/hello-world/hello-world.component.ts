import { Component } from '@angular/core';

@Component({
  selector: 'tweempus-hello-world',
  template: `<p>{{ text }}</p>`,
  styles: ['p{color: red;}']
})
export class HelloWorldComponent{
  text = "Hola mundo"
}
